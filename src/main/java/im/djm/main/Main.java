package im.djm.main;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import im.djm.block.data.Data;
import im.djm.blockchain.BlockChain;

public class Main {
	private static class TestData implements Data {
		private String txtData;

		public TestData(String txtData) {
			this.txtData = txtData;
		}

		@Override
		public String toString() {
			return this.txtData;
		}

		@Override
		public byte[] getRawData() {
			return txtData.getBytes();
		}

	}

	private static List<Data> data = new ArrayList<>();
	static {
		data.add(new TestData("Data 1"));
		data.add(new TestData("Data 2"));
		data.add(new TestData("Data 3"));
		data.add(new TestData("DATA"));
		data.add(new TestData("DATA"));
		data.add(new TestData("DATA"));
		data.add(new TestData("DATA"));
	}

	public static void main(String[] args) throws NoSuchAlgorithmException {

		// create block chain
		BlockChain blockChain = new BlockChain();

		// add a few blocks
		for (Data aData : data) {
			blockChain.add(aData);
		}

		// print content
		System.out.println(blockChain);

	}
}
