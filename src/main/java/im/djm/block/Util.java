package im.djm.block;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Util {

	private Util() {
		throw new IllegalStateException("It is not allowed to call private constructor.");
	}

	public static byte[] concatenateArrays(byte[] arr1, byte[] arr2) {
		byte[] result = new byte[arr1.length + arr2.length];
		System.arraycopy(arr1, 0, result, 0, arr1.length);
		System.arraycopy(arr2, 0, result, arr1.length, arr2.length);

		return result;
	}

	public static byte[] concatenateArrays(byte[]... arrays) {
		byte[] result = new byte[0];
		for (byte[] array : arrays) {
			result = Util.concatenateArrays(result, array);
		}

		return result;
	}

	public static byte[] calculateRawHash(byte[] rawBlock) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(rawBlock);
		byte[] digest = md.digest();

		return digest;
	}

}
