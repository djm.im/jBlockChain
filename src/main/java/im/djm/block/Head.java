package im.djm.block;

import com.google.common.primitives.Longs;

/**
 * @author djm.im
 */
public class Head {
	private final Hash prevHash;

	private final long length;

	private long nonce;

	private int difficulty;

	private long timestamp;

	private Hash dataHash;

	public Head(Hash prevHash, long lenght, Hash hashData) {
		this.timestamp = System.currentTimeMillis() / 1000;

		this.prevHash = prevHash;
		this.length = lenght;

		this.dataHash = hashData;
	}

	/**
	 * @return
	 */
	public byte[] getRawHead() {
		byte[] rawPrevHash = new byte[32];
		if (this.prevHash != null) {
			rawPrevHash = this.prevHash.getRawHash();
		}

		byte[] rawLength = Longs.toByteArray(this.length);
		byte[] rawNonce = Longs.toByteArray(this.nonce);
		byte[] rawDifficulty = Longs.toByteArray(this.difficulty);
		byte[] rawTimestamp = Longs.toByteArray(this.timestamp);
		byte[] rawDataHash = this.dataHash.getRawHash();

		byte[] rawHead = Util.concatenateArrays(rawPrevHash, rawLength, rawNonce, rawDifficulty, rawTimestamp,
				rawDataHash);

		return rawHead;
	}

	/**
	 * @return
	 */
	public Hash getPrevHash() {
		return this.prevHash;
	}

	/**
	 * 
	 * @return
	 */
	public long getLength() {
		return this.length;
	}

	/**
	 * 
	 */
	public void incNonce() {
		this.nonce++;
	}

	/**
	 * 
	 * @return
	 */
	public int getDifficulty() {
		return this.difficulty;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(System.lineSeparator());

		sb.append("  Prev: " + this.prevHash);
		sb.append(System.lineSeparator());

		sb.append("  Length: " + this.length);
		sb.append(System.lineSeparator());

		sb.append("  Nonce: " + this.nonce);
		sb.append(System.lineSeparator());

		sb.append("  Difficulty: " + this.difficulty);
		sb.append(System.lineSeparator());

		sb.append("  Timestamp: " + this.timestamp);
		sb.append(System.lineSeparator());

		sb.append("  Data Hash: " + this.dataHash);
		sb.append(System.lineSeparator());

		sb.append("}");

		return sb.toString();
	}

}
