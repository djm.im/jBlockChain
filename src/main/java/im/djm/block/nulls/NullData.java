package im.djm.block.nulls;

import im.djm.block.data.Data;

/**
 * 
 * @author djm.im
 *
 */
public final class NullData implements Data {

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Null Data";
	}

}
