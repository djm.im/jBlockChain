package im.djm.block.nulls;

import java.security.NoSuchAlgorithmException;

import im.djm.block.Block;

/**
 * 
 * @author djm.im
 *
 */
public final class NullBlock extends Block {

	/**
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	public NullBlock() throws NoSuchAlgorithmException {
		/// TODO: 1510903985
		super(new NullData(), new NullHash());
	}

}
