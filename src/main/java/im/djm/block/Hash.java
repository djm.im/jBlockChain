/**
 * @author djm.im
 */
package im.djm.block;

import java.util.Arrays;

/**
 * @author djm.im
 *         <p>
 *         This is wrapper class for byte[]
 *         <p>
 *         TODO: the class should be immutable
 */
public class Hash {
	private byte[] content;

	private int hashCode;

	private int binaryLeadingZeros;

	/**
	 * @param bytes
	 */
	public Hash(byte[] bytes) {
		this.content = new byte[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			this.content[i] = bytes[i];
		}

		this.hashCode = Arrays.hashCode(this.content);
		this.binaryLeadingZeros = countBinaryLeadingZeros();
	}

	private int countBinaryLeadingZeros() {
		int counter = 0;

		int index;
		for (index = 0; index < this.content.length; index++) {
			byte val = this.content[index];
			if (val != 0) {
				break;
			}
			counter += 8;
		}
		if (index < this.content.length) {
			counter += countInByte(this.content[index]);
		}

		return counter;
	}

	private int countInByte(byte aByte) {
		int count = 0;

		byte mask = (byte) 0B1000_0000;

		while ((aByte & mask) == 0 && mask != 1) {
			count++;
			mask >>= 1;
		}

		return count;
	}

	/**
	 *
	 */
	@Override
	public int hashCode() {
		return this.hashCode;
	}

	/**
	 *
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (this.getClass() != obj.getClass()) {
			return false;
		}

		Hash other = (Hash) obj;

		byte[] bytes = other.content;
		if (this.content == null) {
			if (bytes == null) {
				return true;
			} else {
				return false;
			}
		} else {
			if (bytes == null) {
				return false;
			} else {
				if (this.content.length != bytes.length) {
					return false;
				}

				// TODO:
				// Replace for loop with
				// Arrays.equals(arr1, arr2);

				for (int i = 0; i < this.content.length; i++) {
					if (this.content[i] != bytes[i]) {
						return false;
					}
				}
				return true;

			}
		}
	}

	/**
	 * @return Method returns byte array copy.
	 */
	public byte[] getRawHash() {
		return Arrays.copyOf(this.content, this.content.length);
	}

	/**
	 * 
	 * @return
	 */
	public int getBinaryLeadingZeros() {
		return this.binaryLeadingZeros;
	}

	/**
	 * @return Method returns hexadecimal representation of hash.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("0x");
		for (byte aByte : this.content) {
			sb.append(String.format("%02X", aByte));
		}

		return sb.toString();
	}

}
