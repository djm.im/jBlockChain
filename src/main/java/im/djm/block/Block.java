package im.djm.block;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.function.Predicate;

import im.djm.block.data.Data;
import im.djm.block.data.Validator;
import im.djm.block.nulls.NullData;
import im.djm.block.nulls.NullHash;

/**
 * @author djm.im
 */
public class Block {

	private final Head head;

	private final Data data;

	private final Hash hash;

	/**
	 * 
	 * @param prevBlock
	 * @param data
	 * @throws NoSuchAlgorithmException
	 */
	public Block(final Block prevBlock, final Data data) throws NoSuchAlgorithmException {
		this.data = data;

		this.head = createHead(prevBlock, data);

		this.hash = calculateHash(this.getRawBlock());
	}

	private static Head createHead(Block prevBlock, Data data) throws NoSuchAlgorithmException {
		Hash prevHash = prevBlock.getHash();

		long length = prevBlock.getLength() + 1;
		Hash dataHash = calculateHash(data.getRawData());

		return new Head(prevHash, length, dataHash);
	}

	/**
	 * Special constructor to create the first block - genesis (null) block.
	 */
	protected Block(NullData nullData, NullHash prevHash) throws NoSuchAlgorithmException {
		// TODO; 1510903985
		this.data = nullData;

		Hash dataHash = calculateHash(data.getRawData());
		this.head = new Head(prevHash, 0, dataHash);

		this.hash = calculateHash(this.getRawBlock());
	}

	private Block(Head head, Data data, Hash hash) {
		this.head = head;
		this.data = data;
		this.hash = hash;
	}

	private static Hash calculateHash(byte[] rawBlock) throws NoSuchAlgorithmException {
		byte[] hashBytes = Util.calculateRawHash(rawBlock);
		return new Hash(hashBytes);
	}

	/**
	 * @return Method returns hash for the block.
	 */
	public Hash getHash() {
		// TODO ?? Should it return copy?
		return this.hash;
	}

	/**
	 * @return Method returns hash of previous block.
	 */
	public Hash getPrevHash() {
		return this.head.getPrevHash();
	}

	private byte[] getRawBlock() {
		byte[] rawHead = this.head.getRawHead();
		byte[] rawData = this.data.getRawData();

		return Util.concatenateArrays(rawHead, rawData);
	}

	/**
	 * 
	 * @return
	 */
	public long getLength() {
		return this.head.getLength();
	}

	/**
	 * 
	 * @return
	 */
	public Data getData() {
		return this.data;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(System.lineSeparator());
		String dashes = "--------------------------------------------------------------------------------------------------------------------------------";
		sb.append("[[" + dashes);
		sb.append(System.lineSeparator());

		sb.append("Head: ");
		sb.append(this.head.toString());
		sb.append(System.lineSeparator());

		sb.append("Data: ");
		sb.append(this.data.toString());
		sb.append(System.lineSeparator());

		sb.append("Hash: ");
		sb.append(this.hash.toString());
		sb.append(System.lineSeparator());

		sb.append(dashes + "]]");
		sb.append(System.lineSeparator());
		sb.append(System.lineSeparator());

		return sb.toString();
	}

	/**
	 * 
	 * @param prevBlock
	 * @param data
	 * @param hashValidator
	 * @param rules
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static Block createNewBlock(Block prevBlock, Data data, Validator<Hash> hashValidator,
			List<Predicate<Hash>> rules) throws NoSuchAlgorithmException {

		Head head = createHead(prevBlock, data);

		byte[] rawData = data.getRawData();

		byte[] rawBlock = Util.concatenateArrays(head.getRawHead(), rawData);

		Hash hash = calculateHash(rawBlock);

		while (!hashValidator.isValid(hash, rules)) {
			head.incNonce();
			rawBlock = Util.concatenateArrays(head.getRawHead(), rawData);
			hash = calculateHash(rawBlock);
		}

		return new Block(head, data, hash);
	}

}
