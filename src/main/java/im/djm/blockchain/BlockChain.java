package im.djm.blockchain;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import im.djm.block.Block;
import im.djm.block.Hash;
import im.djm.block.data.Data;
import im.djm.block.data.Validator;
import im.djm.block.nulls.NullBlock;

/**
 * @author djm.im
 */
public class BlockChain {

	private Map<Hash, BlockWrapper> blocks = new HashMap<>();

	private BlockWrapper topBlockWrapper;

	private static Validator<Data> dataValidator = new Validator<Data>() {
	};
	private static List<Predicate<Data>> dataValidationRules = new ArrayList<>();
	static {
		dataValidationRules.add(data -> data != null);
		dataValidationRules.add(data -> true);
	}

	private static Validator<Block> blockValidator = new Validator<Block>() {
	};
	private List<Predicate<Block>> blockValidationRules = new ArrayList<>();

	private static

	Validator<Hash> hashValidator = new Validator<Hash>() {
	};
	private static List<Predicate<Hash>> hashValidationRules = new ArrayList<>();
	static {
		hashValidationRules.add(hash -> hash != null);
		hashValidationRules.add(hash -> hash.getBinaryLeadingZeros() >= 16);
	}

	/**
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	public BlockChain() throws NoSuchAlgorithmException {
		this.initNullBlock();
		this.initBlockValidationRules();
	}

	private void initNullBlock() throws NoSuchAlgorithmException {
		// the null block has to be the same for in all nodes
		NullBlock nullBlock = new NullBlock();

		BlockWrapper nullBlockWrapper = new BlockWrapper(nullBlock, null);
		Hash nullKey = nullBlock.getHash();
		this.blocks.put(nullKey, nullBlockWrapper);
		this.topBlockWrapper = nullBlockWrapper;
	}

	private void initBlockValidationRules() {
		blockValidationRules.add(block -> block != null);

		// Validate data
		blockValidationRules.add(block -> {
			return BlockChain.dataValidator.isValid(block.getData(), BlockChain.dataValidationRules);
		});

		// check if previous block exist in block chain
		blockValidationRules.add(block -> {
			Hash prevHashKey = block.getPrevHash();
			BlockWrapper prevBlock = this.blocks.get(prevHashKey);
			if (prevBlock == null) {
				return false;
			}
			return true;
		});

		// check if `block` already exist in map
		blockValidationRules.add(block -> {
			Hash theBlockKey = block.getHash();
			BlockWrapper theBlock = this.blocks.get(theBlockKey);
			if (theBlock != null) {
				return false;
			}
			return true;
		});

	}

	/**
	 * 
	 * @param block
	 * @return
	 * 
	 * 		Add block that already exists.
	 */
	public boolean add(Block block) {
		if (BlockChain.blockValidator.isValid(block, this.blockValidationRules)) {
			Hash mapKey = block.getHash();

			Hash prevBlockHash = block.getPrevHash();
			BlockWrapper prevBlockWrapper = this.blocks.get(prevBlockHash);
			if (prevBlockWrapper == null) {
				return false;
			}

			BlockWrapper blockWrapper = new BlockWrapper(block, prevBlockWrapper);
			this.blocks.put(mapKey, blockWrapper);
			this.topBlockWrapper = blockWrapper;

			return true;
		}

		return false;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Print blockchain from top to bottom (genesis block).");
		sb.append(System.lineSeparator());

		BlockWrapper currentBlockWrapper = this.topBlockWrapper;
		do {
			sb.append(currentBlockWrapper.getBlock());

			currentBlockWrapper = currentBlockWrapper.getPrevBlockWrapper();
		} while (currentBlockWrapper != null);

		return sb.toString();
	}

	/**
	 * 
	 * @param data
	 * @throws NoSuchAlgorithmException
	 */
	public void add(Data data) throws NoSuchAlgorithmException {
		if (!BlockChain.dataValidator.isValid(data, BlockChain.dataValidationRules)) {
			// TODO: throw exception
			// Cannot add invalid data
			return;
		}

		Block block = generateNewBlock(data);
		this.add(block);
	}

	private Block generateNewBlock(Data data) throws NoSuchAlgorithmException {
		Block prevBlock = this.getTopBlock();
		Block block = Block.createNewBlock(prevBlock, data, BlockChain.hashValidator, BlockChain.hashValidationRules);

		return block;
	}

	/**
	 * 
	 * @return
	 */
	public Block getTopBlock() {
		return this.topBlockWrapper.getBlock();
	}

}
